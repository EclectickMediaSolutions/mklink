# Description

This utility provides a simple method to expose a script to the network.

The `mklink.sh` utility makes a link using `socat` to the provided `PORT` and `SCRIPT`.

Essentially simply implements an infinite loop that spawns a new process of `socat` each iteration. This is done to ensure that it never quits trying to run, regardless of what happens. Let the fools sort out the errors later.

Due to the infinite loop, the package also implements the `rmlink.sh` utility to "automagically" remove all processes associated with the specified script that are being maintained by `mklisten.sh`.

# Usage:

> `mklisten.sh`

```
usage: mklisten [-h] PORT SCRIPT

      PORT     The port to use for script communication... Must be between 9000 & 9100
    SCRIPT     The script to execute. Must be valid script with execution bit.
        -h     Display this help message and exit
```

---

> `rmlisten.sh`

```
usage: rmlisten [-h] SCRIPT

    SCRIPT     The script to attempt to remove.
        -h     Display this help message and exit
```

# Installation:

To make installation easy, first ensure that you have `socat` installed. If you don't' you can obtain it on most Linux systems via the following:

> `sudo apt-get install socat`

After you have `socat` installed, the scripts will run on their own.

For even more convienience, append the following lines to your `.bashrc`:

```bash
alias mklisten=/path/to/script/mklisten.sh
alias rmlisten=~/path/to/script/rmlisten.sh
alias mkli='mklisten'
alias rmli='rmlisten'
```

This assigns an alias to the scripts at both `mklisten` and `rmlisten`, and `mkli` and `rmli`
