#! /bin/bash
# Copyright Ariana Giroux, February 2018 under the MIT license
# 
# Provides a convienient way to disable services created with the mklisten.sh utility.

# Define printable docs
usage () {
  echo "usage: rmlisten [-h] SCRIPT"
  echo ""
  echo "    SCRIPT     The script to attempt to remove."
  echo "        -h     Display this help message and exit"
  exit 1
}

if [ "$1" == "-h" ]; then  # If the first argument is -h
  usage
  exit 1
fi

if [ $# -lt 1 ]; then  # If there aren't enough arguments to operate
  echo "usage: rmlisten [-h] SCRIPT"
  exit 1
fi

# First, retrieve PIDs that have both the script ($1) and socat in the invocation
socat_pids=$(\
  ps -ax | grep $1 | grep socat | grep -v grep | sed 's/ .*//'\
)

# Secondly, retrieve PIDs that have both the script ($1) and mklisten.sh in the invocation
mklisten_pids=$(\
  ps -ax | grep $1 | grep mklisten.sh | grep -v grep | sed 's/ .*//'\
)

echo "Killing mklisten for $1"  # notify user
kill $socat_pids $mklisten_pids &> /dev/null


# vim:set nospell:
