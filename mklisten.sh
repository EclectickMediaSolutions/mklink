#! /bin/bash
# Copyright Ariana Giroux, February 2018 under the MIT license
# 
# Provides simple port and script linking services, making for easier utility networking.
# Pairs with the rmlisten.sh utility.

# Define printable docs
usage () {
  echo "usage: mklisten [-h] PORT SCRIPT"
  echo ""
  echo "      PORT     The port to use for script communication... Must be between 9000 & 9100"
  echo "    SCRIPT     The script to execute. Must be valid script with execution bit."
  echo "        -h     Display this help message and exit"
  exit 1
}

if [ "$1" == "-h" ]; then  # If the first argument is -h
  usage
fi

if [ $# -lt 2 ]; then  # If there aren't enough arguments to operate
  echo "usage: mklisten [-h] PORT SCRIPT"
  exit 1
fi

if [ $1 -gt 9100 ] && [ $1 -lt 9000 ]; then  # if the port is out of range
  >&2 echo "$1 is not a valid port! Must be greater than 9000 and less than 9100!"
  exit 1
fi

if [ -x $2 ]; then  # If file exists and has execution bit
  echo "Running connection loop for port $1 with script $2"
else
  >&2 echo "$2 is not a valid file!"
  exit 1
fi

while true; do  # Keep executing forever, relying on SIGTERM to force exit.
 /usr/bin/socat tcp-listen:$1,reuseaddr exec:"$2"  # Now lets connect the script ($1) to the port ($2)
done

# vim:set nospell:
